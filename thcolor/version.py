#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2021-2022 Thomas "Cakeisalie5" Touhey <thomas@touhey.fr>
# This file is part of the thcolor project, which is MIT-licensed.
# *****************************************************************************
"""Version definition for the thcolor module."""

__all__ = ['version']

version = '0.4'

# End of file.
