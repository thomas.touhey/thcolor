#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2018-2022 Thomas "Cakeisalie5" Touhey <thomas@touhey.fr>
# This file is part of the thcolor project, which is MIT-licensed.
# *****************************************************************************
"""HTML/CSS-like color parsing, mainly for the `[color]` tag.

Defines the `get_color()` function which returns an rgba value.

The functions in this module do not aim at being totally compliant with
the W3C standards, although it is inspired from it.
"""

# End of file.
