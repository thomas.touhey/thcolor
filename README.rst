thcolor -- the touhey color management module
=============================================

This module is a color management module made by `Thomas Touhey`_ (``th``
is for ``touhey``) for the `textoutpc`_ project, a BBCode to HTML translation
module. It provides the following features:

- color management and conversions between formats (RGB, HSL, HWB, NCol, …).
- text-to-color using close-to-CSS format.

For more information, consult `the official website`_.

For getting started with the module, an onboarding is available on
`the official documentation`_, as well as guides, discussion topics and
references.

.. _Thomas Touhey: https://thomas.touhey.fr/
.. _textoutpc: https://textout.touhey.pro/
.. _the official website: https://thcolor.touhey.pro/
.. _the official documentation: https://thcolor.touhey.pro/docs/
