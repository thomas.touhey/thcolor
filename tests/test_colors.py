#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2019-2021 Thomas "Cakeisalie5" Touhey <thomas@touhey.fr>
# This file is part of the thcolor project, which is MIT-licensed.
# *****************************************************************************
"""Unit tests for the thcolor color decoding and management module."""

import pytest
from thcolor.angles import *  # NOQA
from thcolor.colors import *  # NOQA
from thcolor.utils import round_half_up


class TestSRGBColors:
    """Test the sRGB color conversions."""

    @pytest.mark.parametrize('args', (
        (None, 0, 0),
        (0, None, 0),
        (0, 0, None),
    ))
    def test_invalid_values(self, args):
        """Try to instanciate the class using invalid values."""

        with pytest.raises(ValueError):
            SRGBColor(*args)

    @pytest.mark.parametrize('args,bytes_', (
        ((0, 0, 0), (0, 0, 0)),
        ((.0019, .002, .005), (0, 1, 1)),
        ((.1, .2, .3), (26, 51, 77)),
    ))
    def test_to_bytes(self, args, bytes_):
        """Try converting to bytes and test the rounding up."""

        assert SRGBColor(*args).asbytes() == bytes_

    @pytest.mark.parametrize('args,bytes_', (
        ((0, .003921, .003925), (0, 1, 1)),
    ))
    def test_from_bytes(self, args, bytes_):
        """Try converting from bytes and test the rounding up."""

        assert SRGBColor(*args) == SRGBColor.frombytes(*bytes_)

    @pytest.mark.parametrize('name,bytes_', (
        # https://stackoverflow.com/q/8318911
        ('chucknorris', (192, 0, 0)),
        ('ninjaturtle', (0, 160, 0)),
        ('crap', (192, 160, 0)),
        ('grass', (0, 160, 0)),

        # https://bugzilla.mozilla.org/show_bug.cgi?id=121738
        ('navyblue', (160, 176, 224)),

        # http://web.archive.org/web/20050403162633/http://www.jgc.org:80/tsc/index.htm
        # See the "Flex Hex" technique.
        ('FxFxFx', (240, 240, 240)),
        ('#FxFxFx', (240, 240, 240)),
        ('FqFeFm', (240, 254, 240)),

        # http://scrappy-do.blogspot.com/2004/08/little-rant-about-microsoft-internet.html
        ('zqbttv', (0, 176, 0)),
        ('6db6ec49efd278cd0bc92d1e5e072d680', (110, 205, 224)),
    ))
    def test_netscape_decoding(self, name, bytes_):
        """Try decoding colors using Netscape color parsing."""

        assert SRGBColor.fromnetscapecolorname(name).asbytes() == bytes_

    @pytest.mark.parametrize('args,expected', (
        ((0, 0, 255), ('#0000FF',)),
        ((1, 22, 242, .5), (
            '#0116F2',
            'rgba(1, 22, 242, 50%)',
        )),
    ))
    def test_css(self, args, expected):
        assert tuple(SRGBColor.frombytes(*args).css()) == expected

    @pytest.mark.parametrize('args,hsl', (
        ((0, 0, 0), (DegreesAngle(0), 0, 0)),
        ((1, 2, 3), (DegreesAngle(210), .5, .01)),
        ((50, 100, 150), (DegreesAngle(210), .5, .39)),
        ((255, 255, 255), (DegreesAngle(0), 0, 1)),
        ((82, 122, 122), (DegreesAngle(180), .2, .4)),
    ))
    def test_hsl(self, args, hsl):
        hue, sat, lgt, *_ = SRGBColor.frombytes(*args).ashsl()
        assert (hue, round(sat, 2), round(lgt, 2)) == hsl

    @pytest.mark.parametrize('args,hsv', (
        ((0, 0, 0), (DegreesAngle(0), 0, 0)),
        ((255, 255, 255), (DegreesAngle(0), 0, 1)),
        ((0, 255, 0), (DegreesAngle(120), 1, 1)),
        ((0, 120, 0), (DegreesAngle(120), 1, .47)),
        ((73, 33, 86), (DegreesAngle(285), .62, .34)),
        ((10, 20, 30), (DegreesAngle(210), .67, .12)),
    ))
    def test_hsv(self, args, hsv):
        hue, sat, val, *_ = SRGBColor.frombytes(*args).ashsv()
        hue = DegreesAngle(int(hue.asdegrees().degrees))
        assert (hue, round(sat, 2), round(val, 2)) == hsv

    @pytest.mark.parametrize('args,hwb', (
        ((82, 122, 122), (DegreesAngle(180), .32, .52)),
        ((13, 157, 230), (DegreesAngle(200), .05, .1)),
        ((212, 212, 212), (DegreesAngle(0), .83, .17)),
    ))
    def test_hwb(self, args, hwb):
        hue, wht, blk, *_ = SRGBColor.frombytes(*args).ashwb()
        hue = DegreesAngle(int(hue.asdegrees().degrees))
        assert (hue, round(wht, 2), round(blk, 2)) == hwb

    @pytest.mark.parametrize('args,yiq', (
        ((0, 0, 0), (0, 0, 0)),
        ((255, 255, 255), (1, 0, 0)),
        ((0, 255, 0), (.587, -.275, -.523)),
        ((0, 120, 0), (.276, -.129, -.246)),
        ((73, 33, 86), (.2, .0271, .0983)),
        ((10, 20, 30), (.07098, -.03611, .00389)),
    ))
    def test_yiq(self, args, yiq):
        y, i, q, *_ = SRGBColor.frombytes(*args).asyiq()
        print((y, i, q), yiq)

        yiq2 = tuple(map(lambda x: round_half_up(x, 3), (y, i, q)))
        yiq = tuple(map(lambda x: round_half_up(x, 3), yiq))
        assert yiq == yiq2

    @pytest.mark.parametrize('args,cmyk', (
        ((0, 0, 0), (0, 0, 0, 1)),
        ((255, 255, 255), (0, 0, 0, 0)),
        ((0, 255, 0), (1, 0, 1, 0)),
        ((0, 120, 0), (1, 0, 1, .53)),
        ((73, 33, 86), (.15, .62, 0, .66)),
        ((10, 20, 30), (.67, .33, 0, .88)),
    ))
    def test_cmyk(self, args, cmyk):
        c, m, y, k, *_ = SRGBColor.frombytes(*args).ascmyk()
        assert (
            round_half_up(c, 2),
            round_half_up(m, 2),
            round_half_up(y, 2),
            round_half_up(k, 2),
        ) == cmyk


class TestHSLColors:
    """Test the HSL color conversions."""

    @pytest.mark.parametrize('args', (
        (0, 0, 0),
        ('hello', 0, 0),
        (DegreesAngle(0), -.5, 0),
        (DegreesAngle(0), 1.5, 0),
        (DegreesAngle(0), 0, -.5),
        (DegreesAngle(0), 0, 1.5),
    ))
    def test_invalid_values(self, args):
        """Try to instanciate the class using invalid values."""

        with pytest.raises(ValueError):
            HSLColor(*args)

    @pytest.mark.parametrize('args,rgb', (
        ((DegreesAngle(195), 1, .5), (0, 191, 255)),
        ((DegreesAngle(240), 1, .25), (0, 0, 128)),
        ((DegreesAngle(0), 1, .5), (255, 0, 0)),
        ((DegreesAngle(0), 0, 0), (0, 0, 0)),
        ((DegreesAngle(0), 0, 1), (255, 255, 255)),
        ((DegreesAngle(0), 0, .01), (3, 3, 3)),
        ((DegreesAngle(145), .30, .60), (122, 184, 148)),
    ))
    def test_srgb(self, args, rgb):
        r, g, b = HSLColor(*args).assrgb().asbytes()
        assert (r, g, b) == rgb

    @pytest.mark.parametrize('args,expected', (
        ((DegreesAngle(0), 1, .4), (
            '#CC0000',
            'hsl(0deg, 100%, 40%)',
        )),
        ((DegreesAngle(0), .5, 1, .2), (
            '#FFFFFF',
            'rgba(255, 255, 255, 20%)',
            'hsla(0deg, 50%, 100%, 20%)',
        )),
    ))
    def test_css(self, args, expected):
        assert tuple(HSLColor(*args).css()) == expected


class TestHSVColor:
    """Test the HSV color conversions."""

    @pytest.mark.parametrize('args,rgb', (
        ((DegreesAngle(120), 0, 0), (0, 0, 0)),
        ((DegreesAngle(120), .5, .5), (64, 128, 64)),
        ((DegreesAngle(120), 1, .5), (0, 128, 0)),
        ((DegreesAngle(120), .5, 1), (128, 255, 128)),
        ((DegreesAngle(355), .2, .8), (204, 163, 167)),
    ))
    def test_srgb(self, args, rgb):
        assert HSVColor(*args).assrgb().asbytes() == rgb


class TestHWBColors:
    """Test the HWB color conversions."""

    @pytest.mark.parametrize('args,rgb', (
        ((DegreesAngle(145), .48, .28), (122, 184, 148)),
    ))
    def test_srgb(self, args, rgb):
        r, g, b = HWBColor(*args).assrgb().asbytes()
        assert (r, g, b) == rgb

    @pytest.mark.parametrize('args,expected', (
        ((DegreesAngle(127), 0, .5), (
            '#00800F',
            'hwb(127deg, 0%, 50%)',
        )),
    ))
    def test_css(self, args, expected):
        assert tuple(HWBColor(*args).css()) == expected


class TestCMYKColors:
    """Test the CMYK color conversions."""

    @pytest.mark.parametrize('args,rgb', (
        ((0, 0, 0, 0), (255, 255, 255)),
        ((0, 0, 0, 1), (0, 0, 0)),
        ((0, 0, 0, .45), (140, 140, 140)),
        ((0, .25, 0, .45), (140, 105, 140)),
        ((0, .25, .77, .45), (140, 105, 32)),
        ((.02, .04, .06, .08), (230, 225, 221)),
    ))
    def test_srgb(self, args, rgb):
        assert CMYKColor(*args).assrgb().asbytes() == rgb


class TestLABColors:
    """Test the LAB color conversions."""

    @pytest.mark.parametrize('args,lch', (
        # Examples created using the following online converter:
        # http://www.easyrgb.com/en/convert.php

        ((0, 0, 0), (0, 0, DegreesAngle(0))),
        ((.5, -128, 128), (.5, 181.019, DegreesAngle(135))),
    ))
    def test_lch(self, args, lch):
        l, c, h, *_ = LABColor(*args).aslch()
        assert (
            round_half_up(l, 3),
            round_half_up(c, 3),
            h.asdegrees(),
        ) == lch


class TestLCHColors:
    """Test the LCH color conversions."""

    @pytest.mark.parametrize('args,lab', (
        # Examples created using the following online converter:
        # http://www.easyrgb.com/en/convert.php

        ((0, 0, DegreesAngle(0)), (0, 0, 0)),
        ((0, 50, DegreesAngle(0)), (0, 50, 0)),
        ((.5, 50, DegreesAngle(0)), (.5, 50, 0)),
        ((.5, 50, DegreesAngle(235)), (.5, -28.679, -40.958)),
        ((.6, 200, DegreesAngle(146)), (.6, -165.808, 111.839)),
        ((1, 200, DegreesAngle(0)), (1, 200, 0)),
    ))
    def test_lab(self, args, lab):
        l, a, b, *_ = LCHColor(*args).aslab()
        l2, a2, b2 = lab
        assert (
            round_half_up(l, 3),
            round_half_up(a, 3),
            round_half_up(b, 3),
        ) == (
            round_half_up(l2, 3),
            round_half_up(a2, 3),
            round_half_up(b2, 3),
        )


class TestXYZColors:
    """Test the XYZ color conversions."""

    @pytest.mark.parametrize('args', (
        (-.5, 0, 0),
        (1.5, 0, 0),
        (0, -.5, 0),
        (0, 1.5, 0),
        (0, 0, -.5),
        (0, 0, 1.5),
        (None, 0, 0),
        (0, None, 0),
        (0, 0, None),
    ))
    def test_invalid_values(self, args):
        """Try to instanciate the class using invalid values."""

        with pytest.raises(ValueError):
            XYZColor(*args)

    @pytest.mark.parametrize('args,rgb', (
        ((0, 0, 0), (0, 0, 0)),
        ((1, 1, 1), (277, 249, 244)),
        ((.1, .2, .3), (-438, 147, 145)),
        ((1, .5, 0), (378, -103, -153)),
    ))
    def test_rgb(self, args, rgb):
        assert XYZColor(*args).assrgb().asbytes() == rgb

    @pytest.mark.parametrize('args,lab', (
        ((1, .5, 0), (.76069, 111.688, 131.154)),
        ((0, .5, 1), (.76069, -327.885, -35.666)),
        ((.2, .3, .4), (.61654, -37.321, -9.353)),
    ))
    def test_lab(self, args, lab):
        l, a, b, *_ = XYZColor(*args).aslab()
        l2, a2, b2 = lab

        l, a, b = (
            round_half_up(l, 3),
            round_half_up(a, 3),
            round_half_up(b, 3),
        )
        l2, a2, b2 = (
            round_half_up(l2, 3),
            round_half_up(a2, 3),
            round_half_up(b2, 3),
        )

        assert (l, a, b) == (l2, a2, b2)


# TODO: add tests for invalid values in constructors.
# TODO: test YIQ colors.
# TODO: test YUV colors.

# End of file.
