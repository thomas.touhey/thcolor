#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2019-2021 Thomas "Cakeisalie5" Touhey <thomas@touhey.fr>
# This file is part of the thcolor project, which is MIT-licensed.
# *****************************************************************************
"""Unit tests for the `thcolor` Python module."""

# This file is only there to indicate that the folder is a module.
# It doesn't actually contain code.

# End of file.
