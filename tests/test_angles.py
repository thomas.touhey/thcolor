#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2019-2021 Thomas "Cakeisalie5" Touhey <thomas@touhey.fr>
# This file is part of the thcolor project, which is MIT-licensed.
# *****************************************************************************
"""Unit tests for the thcolor color decoding and management module."""

from math import pi

import pytest
from thcolor.angles import *  # NOQA


class TestAngles:
    """Test angle definitions and conversions."""

    @pytest.mark.parametrize('fst,snd', (
        (GradiansAngle(.1), GradiansAngle(.2)),
    ))
    def test_not_equal(self, fst, snd):
        assert fst != snd

    @pytest.mark.parametrize('angle,expected', (
        (DegreesAngle(400), DegreesAngle(40)),
        (DegreesAngle(-10), DegreesAngle(350)),
        (TurnsAngle(-1.5), TurnsAngle(1)),
        (RadiansAngle(6 * pi), RadiansAngle(0)),
        (RadiansAngle(7.5 * pi), RadiansAngle(1.5 * pi)),
        (GradiansAngle(-975), GradiansAngle(225)),
    ))
    def test_principal_angles(self, angle, expected):
        return angle.asprincipal() == expected

    @pytest.mark.parametrize('test_input,expected', (
        ('120deg', DegreesAngle(120)),
        ('5rad', RadiansAngle(5)),
        ('3grad', GradiansAngle(3)),
        ('6.turns', TurnsAngle(6)),
        ('355', DegreesAngle(355)),
    ))
    def test_expr(self, test_input, expected):
        angle = Angle.fromtext(test_input)
        assert isinstance(angle, type(expected))
        assert angle == expected

    @pytest.mark.parametrize('rad,deg', (
        (0, 0),
        (-pi / 2, 270),
        (pi / 2, 90),
        (3 * pi / 4, 135),
        (-3 * pi / 4, 225),
    ))
    def test_radians_to_degrees(self, rad, deg):
        return RadiansAngle(rad).asprincipal().asdegrees().degrees == deg


# End of file.
