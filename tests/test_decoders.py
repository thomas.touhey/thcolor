#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2019-2021 Thomas "Cakeisalie5" Touhey <thomas@touhey.fr>
# This file is part of the thcolor project, which is MIT-licensed.
# *****************************************************************************
"""Unit tests for the thcolor color decoding and management module."""

from typing import Any

import pytest

from thcolor.angles import *
from thcolor.colors import *
from thcolor.decoders import *
from thcolor.errors import *


class TestInvalidDecoders:
    """Test the exceptions raised when an invalid decoder is created."""

    def test_unknown_data_type(self):
        with pytest.raises(TypeError, match=r'neither an alias'):
            class BogusDecoder(MetaColorDecoder):
                hello = 'hello'

    def test_circular_aliases(self):
        with pytest.raises(ValueError, match=r'cyclic dependency'):
            class BogusDecoder(MetaColorDecoder):
                a = alias('b')
                b = alias('a')

    def test_alias_unknown_argument(self):
        with pytest.raises(ValueError, match=r'not an argument'):
            class BogusDecoder(MetaColorDecoder):
                def f(a: int) -> int:
                    return a

                g = alias('f', args=('b',))

    def test_alias_non_default_argument(self):
        with pytest.raises(ValueError, match=r'left without value'):
            class BogusDecoder(MetaColorDecoder):
                def f(a: int, b: int = 0) -> int:
                    return a + b

                g = alias('f', args=('b',))

    def test_varargs(self):
        with pytest.raises(ValueError, match=r'variable arg'):
            class BogusDecoder(MetaColorDecoder):
                def f(a: int, b: int = 0, *args) -> int:
                    return a + b + sum(args)


class TestBaseDecoder:
    """Test the base decoder with no options enabled."""

    @pytest.fixture
    def decoder(self):
        class StrictDecoder(MetaColorDecoder):
            B20 = 12.34
            heLLo = 56.78

        return StrictDecoder()

    @pytest.mark.parametrize('test_input,expected', (
        ('1 / 2 , 3', (
            1, 2, 3.0,
        )),
        ('20deg 4turn 3,, 3. 3.0 .2 50%', (
            DegreesAngle(20), TurnsAngle(4), 3, None, 3.0, 3.0, 0.2, 0.5,
        )),
        ('#12345F', (
            SRGBColor.frombytes(18, 52, 95, 1.00),
        )),
    ))
    def test_syntax(self, decoder, test_input, expected):
        """Test basic syntax.

        Tests syntaxes that are always available, with separators.
        """

        result = decoder.decode(test_input)
        assert result == expected

    @pytest.mark.parametrize('test_input,expected', (
        ('20.5, 20 / 19.5deg', (
            DegreesAngle(20.5), DegreesAngle(20), DegreesAngle(19.5),
        )),
    ))
    def test_coersce_into_angles(self, decoder, test_input, expected):
        """Test the decoding while indicating that we want angles."""

        result = decoder.decode(test_input, prefer_angles=True)
        assert result == expected

    def test_existing_variable(self, decoder):
        """Test getting an existing variable."""

        result = decoder.decode('hello')
        assert result == (56.78,)

    def test_non_existing_variable(self, decoder):
        """Test getting a non-existing variable."""

        with pytest.raises(ColorExpressionSyntaxError, match=r'unknown value'):
            decoder.decode('nonexisting')

    def test_existing_variable_ncol_format(self, decoder):
        """Test decoding an expression using an NCol-like variable.

        We want to avoid the NCol subsytem from shadowing
        existing variables when NCol support is disabled.
        """

        result = decoder.decode('B20')
        assert result == (12.34,)

    def test_non_existing_variable_ncol_format(self, decoder):
        """Test decoding an expression using an NCol-like variable.

        We want to avoid the NCol subsystem intercepting errors
        in case of non-existing variables.
        """

        with pytest.raises(ColorExpressionSyntaxError, match=r'unknown value'):
            decoder.decode('Y40')

    def test_disabled_extended_hex(self, decoder):
        """Test decoding an expression using an extended hex color.

        This should be disabled in this state.
        """

        with pytest.raises(ColorExpressionSyntaxError, match=r'extended hex'):
            decoder.decode('#1234')


class TestExtendedHexDecoder:
    """Test base decoder with extended hex support."""

    @pytest.fixture
    def decoder(self):
        class Decoder(MetaColorDecoder):
            __extended_hex_support__ = True

        return Decoder()

    @pytest.mark.parametrize('test_input,expected', (
        ('#0003', SRGBColor(0, 0, 0, alpha=.2)),
        ('#000A', SRGBColor(0, 0, 0, alpha=.666667)),
    ))
    def test_extended_hex(self, decoder, test_input, expected):
        assert decoder.decode(test_input) == (expected,)


class TestNColDecoder:
    """Test base decoder with ncol support."""

    @pytest.fixture
    def decoder(self):
        class Decoder(MetaColorDecoder):
            __ncol_support__ = True

            def sum2(a: int, b: int) -> int:
                return a + b

            def sum5(a: int, b: int, c: int, d: int, e: int) -> int:
                return a + b + c + d + e

            def col(a) -> Any:
                return a

        return Decoder()

    @pytest.mark.parametrize('test_input,expected', (
        (
            'B20 / 50% 32%',
            HWBColor(DegreesAngle(252), .5, .32, 1.00),
        ),
        (
            'B20 /  / 16%',
            HWBColor(DegreesAngle(252), 0, .16, 1.00),
        ),
        (
            'Y40, 33%, 55%',
            HWBColor(DegreesAngle(84), 0.33, 0.55, 1.00),
        ),
        (
            'Y40, sum2(35, -2), 55%',
            HWBColor(DegreesAngle(84), 0.33, 0.55, 1.00),
        ),
        (
            'Y40, sum5(1, 1, 1, 1, 29), 55%',
            HWBColor(DegreesAngle(84), 0.33, 0.55, 1.00),
        ),
        (
            'Y40',
            HWBColor(DegreesAngle(84), 0, 0),
        ),
        (
            'col(Y40)',
            HWBColor(DegreesAngle(84), 0, 0),
        ),
    ))
    def test_ncol(self, decoder, test_input, expected):
        """Test natural colors."""

        result = decoder.decode(test_input)
        assert result == (expected,)

    def test_invalid_ncol(self, decoder):
        with pytest.raises(ColorExpressionSyntaxError, match=r'unknown value'):
            assert decoder.decode('Y105, 50%, 50%')


class TestNetscapeDecoder:
    """Test base decoder with netscape color support."""

    @pytest.fixture
    def decoder(self):
        class Decoder(MetaColorDecoder):
            __ncol_support__ = False
            __defaults_to_netscape_color__ = True

        return Decoder()

    @pytest.mark.parametrize('test_input,expected', (
        ('chucknorris', SRGBColor.frombytes(192, 0, 0, 1.00)),
        (
            '6db6ec49efd278cd0bc92d1e5e072d680',
            SRGBColor.frombytes(110, 205, 224),
        ),
    ))
    def test_netscape_colors(self, decoder, test_input, expected):
        """Test decoding using Netscape colors."""

        result = decoder.decode(test_input, prefer_colors=True)
        assert result == (expected,)

    @pytest.mark.parametrize('test_input,expected', (
        ('#123', SRGBColor.frombytes(17, 34, 51, 1.00)),
        ('123', SRGBColor.frombytes(1, 2, 3, 1.00)),
        ('123.0', SRGBColor.frombytes(18, 48, 0, 1.00)),
    ))
    def test_coersce_into_colors(self, decoder, test_input, expected):
        """Test the decoding while indicating that we want colors.

        It is expected from the decoder to use Netscape color name
        style decoding in this scenario.
        """

        result = decoder.decode(test_input, prefer_colors=True)
        assert result == (expected,)


# End of file.
