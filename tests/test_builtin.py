#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2019-2021 Thomas "Cakeisalie5" Touhey <thomas@touhey.fr>
# This file is part of the thcolor project, which is MIT-licensed.
# *****************************************************************************
"""Unit tests for the thcolor color decoding and management module."""

import pytest
from thcolor.angles import *  # NOQA
from thcolor.colors import *  # NOQA
from thcolor.decoders import *  # NOQA
from thcolor.builtin import *  # NOQA
from thcolor.errors import *  # NOQA


class TestDefaultDecoder:
    """Test the default decoder with all features."""

    @pytest.fixture
    def decoder(self):
        return DefaultColorDecoder()

    @pytest.mark.parametrize('test_input,expected', (
        ('blue', SRGBColor.frombytes(0, 0, 255, 1.00)),
        ('rgb(110%, 0%, 0%)', SRGBColor(1.0, 0, 0, 1.00)),
        ('rgb(1, 22,242)', SRGBColor.frombytes(1, 22, 242, 1.00)),
        (' rgb (1,22, 242 , 50.0% )', SRGBColor.frombytes(1, 22, 242, 0.50)),
        (' rgb (1 22/ 242,50.0%,/)', SRGBColor.frombytes(1, 22, 242, 0.50)),
        ('rgba(1,22,242,0.500)', SRGBColor.frombytes(1, 22, 242, 0.50)),
        ('rbga(5, 7)', SRGBColor.frombytes(5, 0, 7, 1.00)),
        ('hsl(0, 1,50.0%)', HSLColor(DegreesAngle(0.0), 1.0, 0.5, 1.00)),
        (
            'hls(0 / 1 0.5   , 0.2)',
            HSLColor(DegreesAngle(0.0), 0.5, 1.0, 0.20),
        ),
        ('hwb(0 0% 0)', HWBColor(DegreesAngle(0), 0.0, 0.0, 1.00)),
        ('hbw(127 .5)', HWBColor(DegreesAngle(127), 0.0, 0.5)),
        ('gray(100)', SRGBColor.frombytes(100, 100, 100, 1.00)),
        ('gray(100 / 55 %)', SRGBColor.frombytes(100, 100, 100, 0.55)),
        ('gray(red( #123456 )/0.2/)', SRGBColor.frombytes(18, 18, 18, 0.20)),
        ('B20 50% 32%', HWBColor(DegreesAngle(252), .5, .32, 1.00)),
        ('ncol(B20 / 50% 32%)', HWBColor(DegreesAngle(252), .5, .32, 1.00)),
        ('cmyk(0% 37% 0.13 .78)', CMYKColor(0, .37, .13, .78, 1.00)),
        (
            'darker(10%,  hsl(0, 1, 50.0%))',
            HSLColor(DegreesAngle(0), 1.00, 0.40, 1.00),
        ),
        (
            'lighter(50%, hsl(0, 1, 60.0%))',
            HSLColor(DegreesAngle(0), 1.00, 1.00, 1.00),
        ),
        (
            'saturate(10%,  hls(0, 1, 85.0%))',
            HSLColor(DegreesAngle(0), 0.95, 1.00, 1.00),
        ),
        (
            'desaturate(10%, hls(0turn, 1, 5%, 0.2))',
            HSLColor(DegreesAngle(0), 0.00, 1.00, 0.20),
        ),
        (
            'rgba(255, 0, 0, 20 %)',
            HSLColor(DegreesAngle(0), 1.00, 0.50, 0.20),
        ),
        (
            'Y40, 33%, 55%',
            HWBColor(DegreesAngle(84), 0.33, 0.55, 1.00),
        ),
        ('cmyk(0% 37% 0.13 .78)', CMYKColor(0.00, 0.37, 0.13, 0.78, 1.00)),
    ))
    def test_decoding_colors(self, decoder, test_input, expected):
        result = decoder.decode(test_input, prefer_colors=True)
        assert result[0] == expected

# End of file.
