API Reference
=============

If you are looking for information on a specific function, class or method,
this part of the documentation is for you.

.. toctree::

    api/errors
    api/angles
    api/colors
    api/decoders
    api/builtin
