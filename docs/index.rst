Welcome to thcolor's documentation!
===================================

thcolor is a color management module made by `Thomas Touhey`_
(``th`` is for ``touhey``).

It was originally made for the `textoutpc`_ project, a BBCode to HTML
translation module.

thcolor allows you to:

* Represent colors using various colorspaces and coordinates.
* Convert between these representations.
* Parse colors and related elements using expressions inspired by (and
  compatible with given the right options) CSS standards by the W3C.
* Make CSS expressions out of given color representations.

For more information, you can consult:

* The project homepage at `thcolor.touhey.pro`_.
* The PyPI project page at `pypi.org`_.
* The source at `forge.touhey.org`_.

The documentation contents is the following:

.. toctree::
	:maxdepth: 2

	onboarding
	guides
	discuss
	api

.. _Thomas Touhey: https://thomas.touhey.fr/
.. _textoutpc: https://textout.touhey.pro/
.. _thcolor.touhey.pro: https://thcolor.touhey.pro/
.. _pypi.org: https://pypi.org/project/thcolor/
.. _forge.touhey.org: https://forge.touhey.org/thcolor.git/
