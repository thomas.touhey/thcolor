Decoders API
============

.. py:module:: thcolor.decoders

The ``thcolor.decoders`` module defines base utilities for decoding color
expressions.

.. autoclass:: ColorDecoder
    :members: decode

For convenience, the following class is defined to define color decoders:

.. autoclass:: MetaColorDecoder

.. autoclass:: alias

.. autofunction:: fallback

See :ref:`defining-decoders` for more information about how to use these
classes.
