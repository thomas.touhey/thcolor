Errors API
==========

.. py:module:: thcolor.errors

In this section, are defined all non-standard errors that are raised by
all submodules.

.. autoclass:: ColorExpressionSyntaxError
    :members: text, column, func
