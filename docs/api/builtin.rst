Builtin API
===========

.. py:module:: thcolor.builtin

The following color decoders are defined:

.. autoclass:: CSS1ColorDecoder

.. autoclass:: CSS2ColorDecoder

.. autoclass:: CSS3ColorDecoder

.. autoclass:: CSS4ColorDecoder

.. autoclass:: DefaultColorDecoder
