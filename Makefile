#!/usr/bin/make -f
 DNAME := dist/$(shell ./setup.py --name)-$(shell ./setup.py --version).tar.gz

test tests:
	@pytest -s

docs:
	@./setup.py build_sphinx

dist: $(DNAME)
$(DNAME):
	@./setup.py sdist

upload: $(DNAME)
	@twine upload $(DNAME)

.PHONY: test tests dist docs

# End of file.
